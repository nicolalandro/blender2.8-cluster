from flask import Flask, render_template, jsonify

from server import rq_job_controller

app = Flask(__name__)
app.config['RQ_REDIS_URL'] = 'redis://redis:6379/0'
rq_job_controller.rq.init_app(app)

JOB_LIST = []


@app.route('/')
def index():
    return render_template("simple.html")


@app.route('/api/job_status')
def job_status():
    job_status = []
    for job in JOB_LIST:
        job_status.append({'id': job.id, 'status': job.get_status(), 'results': job.result})
    return jsonify(job_status)


@app.route('/api/exec_render')
def encueue_jobs():
    input_path = '../spec/fixtures/sedia.blend'
    output_path = './'
    start = 5
    end = 7
    job = rq_job_controller.exec_img_job.queue(input_path, start, end, output_path, queue='exec_queue', timeout=-1)
    JOB_LIST.append(job)
    return str(job.id)
