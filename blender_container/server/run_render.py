import os
import bpy.ops
import logging

logger = logging.getLogger()
logger.setLevel(20)

start = os.getenv('START')
end = os.getenv('END')
out = os.getenv('OUT')

if start != None and end != None and out != None:
    scene = bpy.context.scene

    # scene.render.image_settings.file_format = 'PNG'
    print(scene.frame_start, scene.frame_end)

    for x in range(int(start), int(end) + 1):
        scene.frame_set(x)
        scene.render.filepath = f"{out}/image_{x}.png"
        bpy.ops.render.render(write_still=1)

    bpy.ops.wm.quit_blender()
else:
    logger.error('Get environment variable OUT, START and END')
    bpy.ops.wm.quit_blender()
