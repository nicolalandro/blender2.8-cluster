import os
from subprocess import Popen, PIPE

from flask_rq2 import RQ

rq = RQ()


@rq.job('exec_queue', timeout=-1)
def exec_img_job(input_file_name, start, end, output_file_name):
    command = [
        '../blender-2.82a/blender',
        '-noaudio',
        '--background',
        '%s' % input_file_name,
        '-P',
        'run_render.py'
    ]

    my_env = os.environ.copy()
    my_env["START"] = str(start)
    my_env['END'] = str(end)
    my_env['OUT'] = output_file_name

    out = Popen(command, stdout=PIPE, stderr=PIPE, env=my_env)

    out_string = out.stdout.read().decode('utf-8')
    err_string = out.stderr.read().decode('utf-8')
    if err_string != '':
        out_string = out_string + '---------ERR---------' + err_string
    return out_string
