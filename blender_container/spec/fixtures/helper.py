import os


def create_path_from_root(file_path):
    base_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    return os.path.join(base_path, file_path)
