import os
import shutil

from mamba import description, it, before, after, _it
from expects import expect, equal
from ..fixtures.helper import create_path_from_root

if __name__ == '__main__':
    os.system('mamba %s' % __file__)
    exit()

with description('Blender script') as self:
    with before.each:
        self.output_folder = create_path_from_root('spec/output_test_folder/')
        self.blender_path = create_path_from_root('blender-2.80/blender')
        self.blender_project_path = create_path_from_root('spec/fixtures/sedia.blend')
        self.script_path = create_path_from_root('server/run_render.py')
        if os.path.isdir(self.output_folder):
            shutil.rmtree(self.output_folder)
        os.mkdir(self.output_folder)

    with after.each:
        if os.path.isdir(self.output_folder):
            shutil.rmtree(self.output_folder)

    with it('do not create images if does not know start and end'):
        command = '%s %s ' \
                  '--background -P %s' % (self.blender_path, self.blender_project_path, self.script_path)
        os.system(command)
        output_folder_ls = os.listdir(self.output_folder)
        expect(output_folder_ls).to(equal([]))

    # pending perchè non funziona nel container docker
    with _it('create images from start to end into the output folder'):
        command = 'START=5 END=7 OUT="%s" ' \
                  '%s %s ' \
                  '--background -P %s' \
                  % (self.output_folder, self.blender_path, self.blender_project_path, self.script_path)
        os.system(command)
        output_folder_ls = os.listdir(self.output_folder)
        output_folder_ls.sort()
        expect(output_folder_ls).to(equal(['image_5.png', 'image_6.png', 'image_7.png']))
