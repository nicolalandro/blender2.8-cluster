[![pipeline status](https://gitlab.com/nicolalandro/blender2.8-cluster/badges/master/pipeline.svg)](https://gitlab.com/nicolalandro/blender2.8-cluster/commits/master)
[![coverage report](https://gitlab.com/nicolalandro/blender2.8-cluster/badges/master/coverage.svg)](https://gitlab.com/nicolalandro/blender2.8-cluster/commits/master)


WIP!!!!

# blender2.8-cluster
The aim of this project is to create a blender cluster in order to do render in parallel on more then one machine.
It start from an oldest [blender-cluster project](https://gitlab.com/nicolalandro/blender-cluster) that work with blender <= 2.79.

* Do not work with docker for this issue: https://devtalk.blender.org/t/blender-2-8-unable-to-open-a-display-by-the-rendering-on-the-background-cycles/8607/12
# DEV

## Setup dev
```bash
# python >= 3.5
# docker
# redis server
cd blender_container
pip install -r requirements.txt
pip install -r dev_requirements.txt

honcho start
```

## Push test container
```bash
cd blender_container
push_test_container.sh
```